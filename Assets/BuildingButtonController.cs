﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildingButtonController : MonoBehaviour
{
    public GameObject player;
    ResourceManager RM;

    public Button buttonOne;
    public GameObject villager;

    [SerializeField]
    private float nextSpawnTime;
    private GameObject noResourcesText2;
    InputManager inputScript;
    BuildingController buildingScript;
    public GameObject selectedObj;
    private Vector3 spawnPosition;

    [SerializeField]
    private float spawnDelay;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        inputScript = player.GetComponent<InputManager>();
        RM = player.GetComponent<ResourceManager>();
        noResourcesText2 = GameObject.Find("No Resources Panel 2");
        noResourcesText2.SetActive(false);

        buttonOne.onClick.AddListener(HireVillager);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void HireVillager()
    {
        if(RM.gold >= 400 && RM.food >= 200 && RM.housing < RM.maxHousing)
        {
            StartCoroutine(VillagerSpawn());
            RM.gold -= 400;
            RM.food -= 200;
            RM.housing += 1;
        } else if (RM.gold < 400 || RM.food < 200 || RM.housing >= RM.maxHousing)
        {
            noResourcesText2.SetActive(true);
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        //my code here after 3 seconds
        noResourcesText2.SetActive(false);
    }

    IEnumerator VillagerSpawn()
    {

         selectedObj = inputScript.selectedObj;
         buildingScript = selectedObj.GetComponent<BuildingController>();
         spawnPosition = buildingScript.location;
         nextSpawnTime = Time.time + spawnDelay;
        yield return new WaitForSeconds(10);
        Instantiate(villager, spawnPosition, Quaternion.identity);
        //Instantiate(villager);
    }
}
