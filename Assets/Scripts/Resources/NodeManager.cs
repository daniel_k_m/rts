﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public enum ResourceTypes { Skymetal, Iron, Steel, Stone, Wood, Food, Gold, Housing }
    public ResourceTypes resourceType;

    // Time villager is at node
    public float harvestTime;
    public float availableResource;

    // indicates when resource is being gathered
    public int gatherers;


    // Start is called before the first frame update
    void Start()
    {
        // Starts the resource tick (means its true)
        StartCoroutine(ResourceTick());   
    }

    // Update is called once per frame
    void Update()
    {
        if(availableResource <= 0)
        {
            // Need to add isGathering = false
            Destroy(gameObject);
        }
    }

    // Ticks down while villager is gathering resource - Adjust with heldResource in GatherTick in Selection Script
    public void ResourceGather()
    {
        if(gatherers != 0)
        {
            availableResource -= gatherers * 5;
        }
    }

    IEnumerator ResourceTick()
    {
        while (true)
        {
            yield return new WaitForSeconds(harvestTime);
            ResourceGather();
        }
    }
}
