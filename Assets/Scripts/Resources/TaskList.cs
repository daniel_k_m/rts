﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tasklist
{
    Gathering,
    Moving,
    Idle,
    Building,
    Attacking,
    Delivering
}