﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownHallController : MonoBehaviour
{
    private float nextSpawnTime;

    [SerializeField]
    private GameObject villagerPrefab;

    [SerializeField]
    private float spawnDelay;

    public bool selected = false;

    GameObject player;
    InputManager inputScript;
    BuildingController buildingScript;

    public GameObject selectedObj;
    private Vector3 spawnPosition;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        inputScript = player.GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void Spawn()
    {
       // selectedObj = inputScript.selectedObj;
     //   buildingScript = selectedObj.GetComponent<BuildingController>();
      //  spawnPosition = buildingScript.location;
     //   nextSpawnTime = Time.time + spawnDelay;
       // Instantiate(villagerPrefab, spawnPosition, Quaternion.identity);
    }

    private bool ShouldSpawn()
    {
        return Time.time >= nextSpawnTime;
    }
}
