﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    // Unit variables
    public string unitType;

    public string unitRank;

    public string unitName;

    public string unitKills;

    public string weapon;
    public string armour;
    public string items;

    public int health;
    public int maxHealth;

    public int energy;
    public int maxEnergy;

    public Sprite icon;

    // Placeable bool
    public bool placeable = true;
    public Vector3 location;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        location = gameObject.transform.position;
    }

    void OnCollisionEnter(Collision other)
    {
        foreach (ContactPoint contact in other.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }
        if (other.gameObject.tag != "Ground")
        {
            placeable = false;
        }
        else
        {
            placeable = true;
        }
        Debug.Log(other);
    }

    void OnCollisionExit(Collision other)
    {
            placeable = true;
        Debug.Log(other);
    }
}
//COPY UNITCONTROLLEr