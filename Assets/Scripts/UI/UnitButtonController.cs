﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UnitButtonController : MonoBehaviour
{
    public GameObject player;
    ResourceManager RM;

    //Buttons
    public Button buttonOne, buttonTwo, buttonThree, buttonFour, buttonFive, buttonSix, buttonSeven, buttonEight, basicBuildings, advancedBuildings, basicBack, advancedBack;

    //Buildings
    public GameObject house;
    public GameObject townHall;
    public GameObject barracks;
    public GameObject fort;
    public GameObject farm;
    public GameObject blacksmith;
    public GameObject lumberMill;
    public GameObject stables;

    private Vector3 mousePosition;

    private GameObject currentPlaceableObject;
    public Material placing;
    public Renderer[] childColors;

    //UI Objects
    private GameObject noResourcesText;

    private CanvasGroup UnitPanel;
    private CanvasGroup BasicBuildingsPanel;
    private CanvasGroup AdvancedBuildingsPanel;
    private CanvasGroup PeasantPanel;

    // Start is called before the first frame update
    void Start()
    {
        BasicBuildingsPanel = GameObject.Find("BasicBuildingsPanel").GetComponent<CanvasGroup>();
        AdvancedBuildingsPanel = GameObject.Find("AdvancedBuildingsPanel").GetComponent<CanvasGroup>();
        PeasantPanel = GameObject.Find("PeasantPanel").GetComponent<CanvasGroup>();

        player = GameObject.FindGameObjectWithTag("Player");
        RM = player.GetComponent<ResourceManager>();
        noResourcesText = GameObject.Find("No Resources Panel");
        noResourcesText.SetActive(false);

        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button

        basicBack.onClick.AddListener(OpenPeasantPanel);
        advancedBack.onClick.AddListener(OpenPeasantPanel);

        basicBuildings.onClick.AddListener(BasicBuildings);
        advancedBuildings.onClick.AddListener(AdvancedBuildings);

        buttonTwo.onClick.AddListener(BuildHouse);
        buttonThree.onClick.AddListener(BuildFarm);
        buttonFour.onClick.AddListener(BuildTownHall);

        buttonFive.onClick.AddListener(BuildBlacksmith);
        buttonSix.onClick.AddListener(BuildLumberMill);
        buttonSeven.onClick.AddListener(BuildStables);
        buttonEight.onClick.AddListener(BuildBarracks);
    }


    // Update is called once per frame
    void Update()
    {
        if (currentPlaceableObject != null)
        {
            //childColors = currentPlaceableObject.GetComponentsInChildren<MeshRenderer>();
            
            //foreach (Renderer color in childColors)
            //{
            //    color.material = placing;
            //}

            if (!EventSystem.current.IsPointerOverGameObject(-1))
            {
                MoveCurrentPlaceableObjectToMouse();
                ReleaseIfClicked();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Destroy(currentPlaceableObject);
            }
        }
    }

    void OpenPeasantPanel()
    {
        PeasantPanel.alpha = 1;
        PeasantPanel.blocksRaycasts = true;
        PeasantPanel.interactable = true;

        BasicBuildingsPanel.alpha = 0;
        BasicBuildingsPanel.blocksRaycasts = false;
        BasicBuildingsPanel.interactable = false;

        AdvancedBuildingsPanel.alpha = 0;
        AdvancedBuildingsPanel.blocksRaycasts = false;
        AdvancedBuildingsPanel.interactable = false;
    }

    void BasicBuildings()
    {
        HidePeasantPanel();
        OpenBasicBuildingsPanel();
    }

    void AdvancedBuildings()
    {
        HidePeasantPanel();
        OpenAdvancedBuildingsPanel();
    }

    void HidePeasantPanel()
    {
        PeasantPanel.alpha = 0;
        PeasantPanel.blocksRaycasts = false;
        PeasantPanel.interactable = false;
    }

    void OpenBasicBuildingsPanel()
    {
        BasicBuildingsPanel.alpha = 1;
        BasicBuildingsPanel.blocksRaycasts = true;
        BasicBuildingsPanel.interactable = true;
    }

    void OpenAdvancedBuildingsPanel()
    {
        AdvancedBuildingsPanel.alpha = 1;
        AdvancedBuildingsPanel.blocksRaycasts = true;
        AdvancedBuildingsPanel.interactable = true;
    }

    void BuildHouse()
    {
        //Output this to console when Button1 or Button3 is clicked
        if (currentPlaceableObject == null && RM.gold >= 200 && RM.Wood >= 200)
        {
            currentPlaceableObject = Instantiate(house);
        } else if (currentPlaceableObject == null && RM.gold < 200 || currentPlaceableObject == null && RM.Wood < 200)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildFarm()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 400 && RM.Wood >= 400)
        {
            currentPlaceableObject = Instantiate(farm);
        }
        else if (currentPlaceableObject == null && RM.gold < 400 || currentPlaceableObject == null && RM.Wood < 400)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildTownHall()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 800 && RM.Wood >= 800)
        {
            currentPlaceableObject = Instantiate(townHall);
        }
        else if (currentPlaceableObject == null && RM.gold < 800 || currentPlaceableObject == null && RM.Wood < 800)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildLumberMill()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 500 && RM.Wood >= 500)
        {
            currentPlaceableObject = Instantiate(lumberMill);
        }
        else if (currentPlaceableObject == null && RM.gold < 500 || currentPlaceableObject == null && RM.Wood < 500)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildStables()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 500 && RM.Wood >= 700)
        {
            currentPlaceableObject = Instantiate(stables);
        }
        else if (currentPlaceableObject == null && RM.gold < 500 || currentPlaceableObject == null && RM.Wood < 700)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildBarracks()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 600 && RM.Wood >= 600)
        {
            currentPlaceableObject = Instantiate(barracks);
        }
        else if (currentPlaceableObject == null && RM.gold < 600 || currentPlaceableObject == null && RM.Wood < 600)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildFort()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 1400 && RM.Wood >= 1400 && RM.stone >= 1400)
        {
            currentPlaceableObject = Instantiate(fort);
        }
        else if (currentPlaceableObject == null && RM.gold < 1400 || currentPlaceableObject == null && RM.Wood < 1400 || currentPlaceableObject == null && RM.stone < 1400)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    void BuildBlacksmith()
    {
        //Output this to console when the Button2 is clicked
        if (currentPlaceableObject == null && RM.gold >= 800 && RM.Wood >= 800)
        {
            currentPlaceableObject = Instantiate(blacksmith);
        }
        else if (currentPlaceableObject == null && RM.gold < 800 || currentPlaceableObject == null && RM.Wood < 800)
        {
            noResourcesText.SetActive(true);
            StartCoroutine(Wait());
        }
        else
        {
            Destroy(currentPlaceableObject);
        }
    }

    private void MoveCurrentPlaceableObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            if (currentPlaceableObject.tag == "House")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 2f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Farm")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 3f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Yard")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.5f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Lumber Yard")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 1.0f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Stables")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.5f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Barracks")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.5f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Fort")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.3f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Blacksmith")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.3f, hitInfo.point.z);
            }
            else if (currentPlaceableObject.tag == "Resource")
            {
                currentPlaceableObject.transform.position = new Vector3(hitInfo.point.x, 0.4f, hitInfo.point.z);
            }

            //currentPlaceableObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
    }

    private void ReleaseIfClicked()
    {
        BuildingController buildingScript = currentPlaceableObject.GetComponent<BuildingController>();
        if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "House")
        {
            RM.gold -= 200;
            RM.Wood -= 200;
            currentPlaceableObject = null;
            RM.houseCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Farm")
        {
            RM.gold -= 400;
            RM.Wood -= 400;
            currentPlaceableObject = null;
            RM.farmCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Yard")
        {
            RM.gold -= 800;
            RM.Wood -= 800;
            currentPlaceableObject = null;
            RM.townHallCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Lumber Yard")
        {
            RM.gold -= 500;
            RM.Wood -= 500;
            currentPlaceableObject = null;
            RM.lumberYardCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Stables")
        {
            RM.gold -= 500;
            RM.Wood -= 700;
            currentPlaceableObject = null;
            RM.lumberYardCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Barracks")
        {
            RM.gold -= 600;
            RM.Wood -= 600;
            currentPlaceableObject = null;
            RM.barracksCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Fort")
        {
            RM.gold -= 1400;
            RM.Wood -= 1400;
            RM.stone -= 1400;
            currentPlaceableObject = null;
            RM.fortCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Resource")
        {
            RM.gold -= 200;
            RM.Wood -= 400;
            currentPlaceableObject = null;
            RM.townHallCount += 1;
        }
        else if (Input.GetMouseButtonDown(0) && currentPlaceableObject.tag == "Blacksmith")
        {
            RM.gold -= 800;
            RM.Wood -= 800;
            currentPlaceableObject = null;
            RM.blacksmithCount += 1;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            currentPlaceableObject = null;
        }
    }

    private void PlayBuildingSound()
    {
        Debug.Log("BUILDING SOUND");
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3);
        //my code here after 3 seconds
        noResourcesText.SetActive(false);
    }

    IEnumerator PlaceBuilding()
    {
        yield return new WaitForSeconds(3);
        //my code here after 3 seconds
        noResourcesText.SetActive(false);
        PlayBuildingSound();
        Debug.Log("PLACED!!!");
    }
}
